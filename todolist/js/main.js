let inputEL = document.querySelector("input");
let btnAddEl = document.querySelector("#addItem");

let listNotes = getLocal();
console.log("listNotes: ", listNotes);

let renderLitsNotes = (listNotes = []) => {
  let contentHTML = "";
  listNotes.forEach((listNote, index) => {
    contentHTML += `<li> <span> ${listNote.name}  </span>
      <span class="icon">
       <i class="fa fa-trash icon--trash " aria-hidden="true" onclick="xoaNote(${index})"></i>
       <i class="fa fa-check  icon--check" aria-hidden="true" onclick="hoanThanh(${index})"></i>
       </span>
      </li>`;
  });
  document.getElementById("todo").innerHTML = contentHTML;
};
renderLitsNotes(listNotes);
btnAddEl.addEventListener(`click`, function () {
  if (!inputEL.value) {
    return false;
  }
  let listNotes = getLocal();
  listNotes.push({ name: inputEL.value });
  inputEL.value = "";
  // localStorage.getItem("listNotes", JSON.stringify(listNotes));
  localStorage.setItem("listNotes", JSON.stringify(listNotes));
  renderLitsNotes(listNotes);
});
// lưu
function getLocal(listNotes) {
  return localStorage.getItem("listNotes")
    ? JSON.parse(localStorage.getItem("listNotes"))
    : [];
}
// xoá
window.xoaNote = (id) => {
  console.log("yes");
  let listNotes = getLocal();
  listNotes.splice(id, 1);
  localStorage.setItem("listNotes", JSON.stringify(listNotes));
  renderLitsNotes(getLocal());
};
// hoàn thành
window.hoanThanh = (id) => {
  let listNotes = getLocal();
  if (listNotes.length > 0) {
    console.log("listNotes: ", listNotes[id]);
    let contentHTML = "";
    Array.from(listNotes).forEach((listNote, index) => {
      contentHTML = `<li> <span> ${listNotes[id].name}  </span>
          <span class="icon">
           <i class="fa fa-trash icon--trash " aria-hidden="true" onclick="xoaNote1(${index})"></i>
           <i class="fa fa-check  icon--check" aria-hidden="true" onclick="hoanThanh(${index})"></i>
           </span>
          </li>`;
    });
    document.getElementById("completed").innerHTML = contentHTML;
  }
};
// za
window.za = () => {
  console.log("ýe");
  let za = getLocal();
  za.reverse();
  console.log("za: ", za);
  renderLitsNotes(za);
};
// az
window.az = () => {
  console.log("ýe");
  let az = getLocal();
  az.sort();
  console.log("az: ", az);
  renderLitsNotes(az);
};
// *****
